package com.es.log.biz;

import com.jcabi.aspects.Loggable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Created by kangkangh on 15/11/29.
 */
public class BizLogger {


	private static final Logger logger = LoggerFactory.getLogger(BizLogger.class);

	public static void main(String[] args) {

		User loginUser = getCurLoginUser();

//      if user has many fields, log the full info of user is unnecessary
//		logger.info("main in BizLogger, curUser is: {} ",loginUser); wrong

		logger.info("main in BizLogger, curUser: id:{}, name:{} ", loginUser.getId(), loginUser.getName());

//      for important process, log start, finish, processing progress info

//	    print log with identity, for example , with userName or request ip
//      (this can be configured in logback.xml using appender's pattern:
//      like this <pattern>[%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %5p %class:%L] %X{userName} %m%n</pattern>)

		Thread thread1 = new Thread(new Runnable() {
			public void run() {
				MDC.put("userName", "user11111111");
				step1("step1");
				step2("step2");
				batchStep3(67);
			}
		});

		Thread thread2 = new Thread(new Runnable() {
			public void run() {
				MDC.put("userName", "user22222222");
				step1("step1");
				step2("step2");
				batchStep3(67);
			}
		});
		thread1.start();
		thread2.start();

	}

	private static User getCurLoginUser() {
		return new User();
	}

//	log start,finish info for process
	public static void step1(String arg) {
		logger.info("for user [{}] step1 start with arg: {}", MDC.get("userName"), arg);
//		 someProcess()
		logger.info("for user [{}] step1 finish ", MDC.get("userName"));
	}

	@Loggable
	public static void step2(String arg) {
//		logger.info("for user [{}] step2 start with arg: {}", MDC.get("userName"), arg);
//		 someProcess()
//		logger.info("for user [{}] step2 finish", MDC.get("userName"));
		return;
	}

//	log progress for process
	public static void batchStep3(int total) {
		logger.info("for user [{}] step3 start with arg: {}", MDC.get("userName"), total);
		int i = 0;
		while (i < total) {
//			doSomeProcess();
			i += 20;
			logger.info("for user [{}] step3 processing, {} processed, total: {}", MDC.get("userName"), Math.min(total, i), total);
		}

		logger.info("for user [{}] step3 finished", MDC.get("userName"));
	}


}
