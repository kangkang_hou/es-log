package com.es.log.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by kangkangh on 15/11/29.
 */
public class ExceptionLogger {


	private static final Logger logger = LoggerFactory.getLogger(ExceptionLogger.class);

	public static void main(String[] args) {
		try {
			service1("arg1", "arg2");
		} catch (IllegalArgumentException e) {
			//  unnecessary to log this exception here
			//  just return it to  frontend user
		} catch (Exception e) {
			logger.error("unknown exception in main: args {}, exception: ", args, e);
		}
	}


	public static void service1(String param1, String param2) {

		service2(Integer.valueOf(param1), param2);

	}


	public static void service2(Integer param1, String param2) {
		try {
//			someProcess();
		} catch (RuntimeException e) {
//			e.printStackTrace();   wrong
//			System.out.print(e);   wrong
//			System.err.print(e);   wrong

//			print the stack trace as well
//			logger.error(e.getMessage())  wrong

//			print the invoke context as well, such as the arguments
//			logger.error("someProcess error in service2: exception:", e);  wrong

//			catch the exception as soon as possible
//          log exception message and throw it to the caller,
//          be sure to throw the exception, unless you know how to deal with it exactly.
			logger.error("someProcess error in service2: args {},{}, exception: ", param1, param2, e);
			throw e;
		}
	}
}
