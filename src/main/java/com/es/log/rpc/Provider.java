package com.es.log.rpc;

import com.google.common.base.Stopwatch;
import com.jcabi.aspects.Loggable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by kangkangh on 15/11/29.
 */
public class Provider {

	private static final Logger logger = LoggerFactory.getLogger(Provider.class);

	// use java code
	public String service(String arg1, String arg2) {

		Stopwatch stopwatch = Stopwatch.createStarted();
		logger.info("service in Provider: args: {},{}", arg1, arg2);

		String result = doSomeProcess(arg1, arg2);

		logger.info("process finished, result: {},  cost {} ms ", result, stopwatch.elapsed(MILLISECONDS));
		return result;
	}


	// use loggable annotation to log args, result and time cost
	@Loggable(value = Loggable.INFO, trim = false)
	public String service2(String arg1, String arg2) {
		return "succeed";
	}

	private String doSomeProcess(String arg1, String arg2) {
		return "";
	}
}
