package com.es.log.rpc;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by kangkangh on 15/11/29.
 */
public class Consumer {


	private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

	public static void main(String[] args) {


		String arg1 = "arg1";
		String arg2 = "arg2";

		// log args, result, time cost around the RPC

		Stopwatch stopwatch = Stopwatch.createStarted();
		logger.info("before invoke service of provider in main: args: {},{}", arg1, arg2);

		Provider provider = new Provider();
		String result = provider.service2(arg1, arg2);
//		String result = provider.service(arg1, arg2);


		logger.info("the result of service is: {}, cost {} ms", result, stopwatch.elapsed(MILLISECONDS));

		return;

	}
}
